//
//  BaseService.swift
//  Smaga1
//
//  Created by Patryk Strzemiecki on 07.09.2016.
//  Copyright © 2016 Patryk Strzemiecki. All rights reserved.
//

import Foundation

var mainAPILink:String = "https://dl.dropboxusercontent.com/" 

class BaseService {
    
    func validateStatusCode(response: NSHTTPURLResponse?) -> Bool {
        let result = self.validateStatusCode(response?.statusCode)
        if result == false {
        }
        return result
    }
    
    func validateStatusCode(statusCode: Int?) -> Bool {
        if statusCode == 400 || statusCode == 401 || statusCode == 403 || statusCode == 404 {
            return false
        } else if statusCode == 500 {
            return false
        }
        return true
    }

}
