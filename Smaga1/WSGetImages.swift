//
//  WSGetImages.swift
//  Smaga1
//
//  Created by Patryk Strzemiecki on 07.09.2016.
//  Copyright © 2016 Patryk Strzemiecki. All rights reserved.
//

import Foundation

import Foundation
import Alamofire

@objc protocol WSGetImagesDelegate {
    optional func getImagesDidStartLoading()
    optional func getImagesDidFailLoadingWithError(error: String)
    func getImagesDidFinishLoading(items: [TestImage])
}

class WSGetImages: BaseService{
    
    var delegate:WSGetImagesDelegate? = nil
    
    class var sharedInstance: WSGetImages {
        struct Static {
            static let instance: WSGetImages = WSGetImages()
        }
        return Static.instance
    }
    
    class func loadTestData(delegate: WSGetImagesDelegate?) {
    
        delegate?.getImagesDidStartLoading?()
        
        Alamofire.request(.GET, mainAPILink + "u/16049878/images/test.json", parameters: nil, headers: nil).responseJSON(options: .MutableContainers) { (response) -> Void in
            
            if let status = response.response?.statusCode {
                print("WSGetImages Status:\(status)")
            }
            if self.sharedInstance.validateStatusCode(response.response?.statusCode) == false {
                delegate?.getImagesDidFailLoadingWithError?("Response not valid!")
                return
            }
            
            var results = [TestImage]()
            switch response.result {
            case .Success(let JSON):

                if !(JSON.objectForKey("lastupdate") is NSNull){
                    print("WSGetImages lastupdate timestamp: ",JSON.objectForKey("lastupdate")!)
                }
                
                if JSON.objectForKey("images") is NSNull{
                } else if let items = JSON.objectForKey("images") as? NSArray {
                    for object in items {
                        let item = TestImage(imagesData: object as! NSDictionary)
                        results.append(item)
                    }
                }
                
            default:
                NSLog("WSGetImages parsing failed")
            }
            delegate?.getImagesDidFinishLoading(results)
        }
        
    }
}