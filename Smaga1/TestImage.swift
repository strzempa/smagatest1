//
//  TestImage.swift
//  Smaga1
//
//  Created by Patryk Strzemiecki on 07.09.2016.
//  Copyright © 2016 Patryk Strzemiecki. All rights reserved.
//

import Foundation

class TestImage: NSObject{
    
    var lastupdate: String? = nil
    var imageURL: String? = nil
    var timestamp: String? = nil
    var imageDescription: String? = nil
    
    init(imagesData: NSDictionary){
        
        if !(imagesData["imageURL"] is NSNull) {
            self.imageURL = (imagesData["imageURL"] as? String) ?? ""
        }
        
        if !(imagesData["info"]?.objectForKey("timestamp") is NSNull) {
            self.timestamp = (imagesData["info"]?.objectForKey("timestamp") as? String) ?? ""
        }
        
        if !(imagesData["info"]?.objectForKey("description") is NSNull) {
            self.imageDescription = (imagesData["info"]?.objectForKey("description") as? String) ?? ""
        }
    }
}