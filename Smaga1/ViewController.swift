//
//  ViewController.swift
//  Smaga1
//
//  Created by Patryk Strzemiecki on 07.09.2016.
//  Copyright © 2016 Patryk Strzemiecki. All rights reserved.
//

import UIKit
import UIColor_Hex_Swift

class ViewController: UIViewController, WSGetImagesDelegate, UITableViewDataSource, UITableViewDelegate {
    
    // MARK: - variables
    
    private var imagesDataArray = [TestImage]()
    
    // MARK: - IBOutlets
    
    @IBOutlet weak var tableView: UITableView!{
        didSet{
            tableView.backgroundColor = UIColor(rgba: "#FB8C00")
            tableView.setEditing(true, animated: true)
            tableView.allowsSelection = true
            tableView.allowsSelectionDuringEditing = true
        }
    }
    
    // MARK: - view methods

    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor(rgba: "#FB8C00")

        tableView.registerClass(CustomCell.self, forCellReuseIdentifier: "CustomCell")
        let nib = UINib(nibName: "CustomCell", bundle: nil)
        self.tableView.registerNib(nib, forCellReuseIdentifier: "CustomCell")
        
        WSGetImages.loadTestData(self)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    // MARK: - UITableView
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.imagesDataArray.count
    }

    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        return CustomCell().loadCell(self.tableView,indexPath: indexPath,imagesDataArray:self.imagesDataArray)
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        CustomCell().handleTapGesture(self.tableView,indexPath: indexPath,imagesDataArray:self.imagesDataArray)
        self.tableView.deselectRowAtIndexPath(indexPath, animated: true)
    }
    
    func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        return true
    }
    
    func tableView(tableView: UITableView, canSelectRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        return true
    }
    
    func tableView(tableView: UITableView, editingStyleForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCellEditingStyle {
        return UITableViewCellEditingStyle.None
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 200.0
    }
    
    func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {}

    func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        return true
    }

    func tableView(tableView: UITableView, moveRowAtIndexPath sourceIndexPath: NSIndexPath, toIndexPath destinationIndexPath: NSIndexPath) {
        swap(&self.imagesDataArray[sourceIndexPath.row], &self.imagesDataArray[destinationIndexPath.row])
        self.tableView.reloadData()
    }
    
    // MARK: - data methods
    
    func getImagesDidFinishLoading(items: [TestImage]){
        self.imagesDataArray = items
        self.reloadAfterWSGetImages()
    }
    
    func reloadAfterWSGetImages(){
        self.tableView.reloadData()
    }

}

