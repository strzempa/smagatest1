//
//  CustomCell.swift
//  Smaga1
//
//  Created by Patryk Strzemiecki on 07.09.2016.
//  Copyright © 2016 Patryk Strzemiecki. All rights reserved.
//

import UIKit
import SDWebImage

class CustomCell: UITableViewCell {
    
    // MARK: - IBOutlets

    @IBOutlet weak var testImage: UIImageView!
    
    // MARK: - cell loading methods
    
    func loadCell(tableView: UITableView,indexPath: NSIndexPath,imagesDataArray:[TestImage]) -> UITableViewCell{
        
        let customCell: CustomCell! = tableView.dequeueReusableCellWithIdentifier("CustomCell", forIndexPath: indexPath) as! CustomCell
        
        if imagesDataArray.count > 0{
            let imageToGet = (imagesDataArray[indexPath.row].imageURL)!
            
            customCell.testImage?.sd_setImageWithURL(NSURL(string: imageToGet), placeholderImage: nil,options: SDWebImageOptions.RetryFailed, progress: {(receivedSize: Int!, expectedSize: Int!) in
                }, completed: { (image, error, cacheType, url) -> Void in
                    print("got image: ",imageToGet)
                    customCell.testImage.image = image
                    let maskingImage = UIImage(named: "star.png")
                    customCell.testImage.image = self.maskImage(image!, mask: maskingImage!)
            })
        }
        return customCell
    }
    
    func maskImage(image:UIImage, mask:(UIImage))->UIImage{
        
        let imageReference = image.CGImage
        let maskReference = mask.CGImage
        
        let imageMask = CGImageMaskCreate(CGImageGetWidth(maskReference),
                                          CGImageGetHeight(maskReference),
                                          CGImageGetBitsPerComponent(maskReference),
                                          CGImageGetBitsPerPixel(maskReference),
                                          CGImageGetBytesPerRow(maskReference),
                                          CGImageGetDataProvider(maskReference), nil, true)
        
        let maskedReference = CGImageCreateWithMask(imageReference, imageMask)
        
        let maskedImage = UIImage(CGImage:maskedReference!)
        
        return maskedImage
    }
    
    func handleTapGesture(tableView: UITableView,indexPath: NSIndexPath,imagesDataArray:[TestImage]){
        if imagesDataArray.count > 0{
            let dateFormatter = NSDateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ"
            dateFormatter.locale = NSLocale(localeIdentifier: "en_US_POSIX")
            let date = dateFormatter.dateFromString( imagesDataArray[indexPath.row].timestamp! )!
            let simpleDate = String(date)
            let alert = UIAlertView()
            alert.title = "Time of the cat is:"
            alert.message = simpleDate.substringToIndex(simpleDate.characters.indexOf("+")!)
            alert.addButtonWithTitle("OK")
            alert.show()
        }
    }
}